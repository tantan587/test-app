import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Link from '../components/Link'
import Copyright from '../components/Copyright';
import getServerHello from '../state/actions/getServerHello'

export default function About() {
  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          Next.js example
        </Typography>
        <Button variant="contained" color="primary" component={Link} naked href="/">
          Go to the main page
        </Button>
        <Button variant="contained" color="secondary" onClick={() => getServerHello()} >
          Get Result From Server in Console
        </Button>
        <Copyright /> 
      </Box>
    </Container>
  );
}