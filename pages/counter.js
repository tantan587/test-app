import React from 'react';
import { connect } from 'react-redux';
import Button from  '@material-ui/core/Button'
import {increment, decrement} from '../state/actions/counterActions'

const Counter = (props) => {
  return <div>
    <Button onClick={increment}>+</Button>
    {props.count}
    <Button onClick={decrement}>-</Button>
  </div>
}

const mapStateToProps = (state) => {
  return {
    count: state.count
  };
}

export default connect(mapStateToProps)(Counter);

