import React from 'react'
import Link from '../components/Link'
import Layout from '../components/layout'
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Copyright from '../components/Copyright';

export default () => (
  <Layout home>
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          Next.js example
        </Typography>
        <Link href="/about" color="secondary">
          <a>Go to the about page</a>
        </Link>
        <Copyright />
        <ul>
        <li>
          <Link href="/b" as="/a">
            <a>a</a>
          </Link>
        </li>
        <li>
          <Link href="/a" as="/b">
            <a>b</a>
          </Link>
        </li>
  </ul>
      </Box>
    </Container>
  </Layout>
)
