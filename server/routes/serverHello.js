const express = require('express')
const router = express.Router()

router.get('/serverHello', (req, res) => {
  console.log('hello from server')
  res.status(200).json({ text: 'Hello From Server' })
})


module.exports = router