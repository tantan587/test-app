import {INCREMENT, DECREMENT} from '../constants/counters'

export const increment = () =>
  ({
    type: INCREMENT,
  })

export const decrement = () =>
({
  type: DECREMENT
})