import { createStore, applyMiddleware  } from 'redux'
import reducers from '../reducers/index'
import thunk from 'redux-thunk'

const clientLogger = store => next => action => {
  console.log(action)
    console.groupCollapsed('dispatching', action.type)
    console.log('prev state', store.getState())
    console.log('action', action)
    let result = next(action)
    console.log('next state', store.getState())
    console.groupEnd()
    return result
}

const middleware = () => [
  clientLogger,
  thunk
]

export default (initialState) => {
  return applyMiddleware(...middleware())(createStore)(reducers, initialState)
}